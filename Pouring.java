import java.util.*;


class Pouring {

	public static void main(String args[]) 

	{

		Scanner s = new Scanner(System.in);

		int jar1size, jar2size, dif, desired;

		int jar1=0, jar2=0;

		jar1size=s.nextInt();

		jar2size=s.nextInt();

		desired=s.nextInt();

		do 

		{

			int temp = 0;

			jar2 += jar1;

			if (jar1 < jar1size )

			{

				for (int i = jar1; i < jar1size; i++)

					temp += 1;

			}

			else

			{

				if (jar1 == jar1size && jar2 <= jar2size) 

				{

					jar1 = 0;

				}

				else 

				{

					dif = jar2 - jar2size;

					jar1 = dif;

					jar2 = jar2size;

				}

			}

			jar1 += temp;

			System.out.println("Jar1:"+jar1+"\t"+"Jar2:"+jar2);

			if(jar2 == jar2size || jar2>jar2size)

			{

				jar2 = 0;

				System.out.println("Jar1:"+jar1+"\t"+"Jar2:"+jar2);

			}

		} while (jar2 != desired);

	}

}
