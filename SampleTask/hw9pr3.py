import random
import sys

# Read content from text.txt file
def readFile( nameOfFile ):
    try:
        file = open( nameOfFile )
        setText = file.read().strip()    # Read the file content without '\n' using strip()
        if setText != '': 
            return setText
        else:
            print ( 'File contain no data, Please insert some text in file' )
            sys.exit()
    except IOError: #Input/Output Exception for handling file
        print ( 'Such directory can not find, So file cannot be opened' )
        sys.exit()
    except EOFError:
        print ( 'Unexpected error:')
        sys.exit()
        

# Add text file into dictionary
def createDictionary( getText ):      
    dict = {} # create empty dictionary 

    sentenceList = getText.split( "." )

    # Suggest starting words in given sentence 
    for sentence in sentenceList:
        joinSentence = sentence.replace(" ", "")
        
        for joinSentenceIndex in range (0, len(joinSentence)):
            dict.setdefault('$', []).append( joinSentence[0] )
            break

    wordList = getText.split(" ")

    # Add keys and values to the dictionary
    for word in wordList:
        if word not in dict:
            for wordIndex in range(0, len( wordList )):
                if ( wordIndex + 1 ) == len( wordList ):
                    break  
                elif word == wordList[ wordIndex ]:
                    lengthOfWord = len( word )
                    if word[ lengthOfWord - 1 ] != '.': # Check the word whethere end of the word or not,
                                                        # if not suggest next to that word to add in dictionary values
                        dict.setdefault( word, []).append( wordList[ wordIndex + 1 ] ) 
 
    return dict   # this way we can use the data in d later!
    
# Generate sentence with suggestion from dictionary
def generateText( getDictionary, noOfTimes ):
    generateRandomWord = []
    randomWord = ( random.choice( getDictionary ["$"] ) )   #Add the starting word of the sentence randomly
    generateRandomWord += [ randomWord ]
    
    for repeatStep in range( 1, noOfTimes ):
        length = len( randomWord )
        if randomWord[length-1] != '.':
            randomWord = (random.choice ( getDictionary[randomWord] ) )
            generateRandomWord += [ randomWord ]
        else:
            randomWord = ( random.choice( getDictionary ["$"] ) )
            generateRandomWord += [ randomWord ]
    return generateRandomWord

# Program start here
textFile = readFile( 'text.txt' )
getDictionary=createDictionary( textFile )

timesToRepeat=10

generatedText = generateText( getDictionary, timesToRepeat )
print ( generatedText ) # Result should print here 

"I love roses and carnations. I hope I get roses for my birthday."
