import sys

# define the function blocks
# Count the no of sentences in text
def sentences(text):
    sentenceCount = 1
    for valueIndex in range (0, len(text)):
        if (valueIndex + 1) == len(text):
            return sentenceCount
        elif text[valueIndex] == "?" and text[valueIndex + 1] == " ":
            sentenceCount += 1
        elif text[valueIndex] == "!" and text[valueIndex + 1] == " ":
            sentenceCount += 1
        elif text[valueIndex] == ".":
            sentenceCount += 1
    return sentenceCount
        
#print(sentences('Wow!?! No way.'))

# Count the no of syllables in text
def syllables(text):
    syllablesCount = 0
    for valueIndex in range (0, len(text)):
        if text[valueIndex] == 'a' or text[valueIndex] == 'i' or text[valueIndex] == 'o' or text[valueIndex] == 'u' or text[valueIndex] == 'y':
            syllablesCount += 1
        elif (valueIndex + 1) == len(text):
            break
        elif text[valueIndex] == 'e' and text[valueIndex + 1] != ' ':
            syllablesCount += 1
    return syllablesCount

#print(syllables('ooneo'))

# Count the no of words in the text
def numWords(text):
    wordsCount = 0
    for valueIndex in range (0, len(text)):
        if text[valueIndex] == ' ' and not text[valueIndex + 1].isdigit():
            wordsCount += 1
        elif (valueIndex + 1) == len(text):
            wordsCount += 1
            break
    return wordsCount

#print(numWords('Wow!?! No 6 hi way.'))

def readabilityIndex(text):
    noOfSentences=sentences(text)
    noOfwords=numWords(text)
    noOfSyllables=syllables(text)

    print ('Number of sentences:',noOfSentences)
    print ('Number of words:',noOfwords)
    print ('Number of syllables:',noOfSyllables)
    
    FI = 0
    FI = int(206.835 - 84.6 * (noOfSyllables / noOfwords) - 1.015 *  (noOfwords  / noOfSentences))
    print ('Number of syllables:',FI)
    return 'Thank You!' 
# map the inputs to the function blocks
options = {1 : sentences,
           2 : numWords,
           3 : syllables,
           4 : readabilityIndex,
}

givenText = (raw_input('Type some text:'))
if (givenText != ' '):
    continueChoice = 'yes'
    while continueChoice == 'yes':
        try:
            menuChoice = float(raw_input("""Welcome to the text readability calculator!
    
    Your options include:

    (1) Count sentences
    (2) Count words
    (3) Count syllables in one word
    (4) Calculate readability
    (5) Quit

    What option would you like?"""))
            
            if menuChoice < 5 and menuChoice >0:
                result = options[menuChoice](givenText)
                print (result)
            else:
                print (' Enter your choice correctly')
            continueChoice = raw_input('''
    Would you like to continue yes/no?''')
        except ValueError:
            print ('Input value error')
            sys.exit()
        
    
    
