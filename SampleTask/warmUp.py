# Check the list if the values are less than the thresh value make it '0'
# updtae list 
def threshold(thresh, List):
    if List == []:
        return [] 
    elif thresh > List[0]:
        List[0] = 0
        newList = List[0]
        return [newList] + threshold(thresh, List[1: ])
    else:
        newList = List[0]
        return [newList] + threshold(thresh, List[1: ])

# Reverse the given list     
def myReverse(List):
    if List == []:            
        return []               
    else:
        firstListValue = List[0] 
        return myReverse(List[1:]) + [firstListValue]

# Remove the given value from list
def myRemove(toRemove, List):
    if List == []:
        return []
    elif toRemove == List[0]:
        return myRemove(toRemove, List[1:])
    else:
        newList = List[0] 
        return [newList] + myRemove(toRemove, List[1:])

# Find the average value from neighbour values update excisting list    
def smooth (oldList):
    myList = []

    for ValueIndex in range (0, len(oldList)):
        if (ValueIndex + 1) == len(oldList):
            newList = (oldList[ValueIndex] + oldList[ValueIndex - 1]) // 2 # Integer division '//'
            myList.insert(ValueIndex, newList)
        elif ValueIndex == 0:
            newList = (oldList[ValueIndex] + oldList[ValueIndex + 1]) // 2 
            myList.insert(ValueIndex, newList)
        else:
            newList = (oldList[ValueIndex - 1] + oldList[ValueIndex] + oldList[ValueIndex + 1]) // 3
            myList.insert(ValueIndex, newList)
    return myList         
    
# Program begins here    
List = [7, 6, 5, 4, 3, 2, 1]

updatedList = threshold(2, List )   # send 2 argument as min value and List
print ( updatedList )

updatedList = myReverse(updatedList)    # send updated list to reverse
print ( updatedList )

updatedList = myRemove(3, updatedList)  # send the value and list- to remove from list
print ( updatedList )

newUpdatedList = smooth (updatedList)   # Find the average value with neighbours
print ( newUpdatedList )
